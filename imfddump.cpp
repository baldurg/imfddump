// imfddump.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

HANDLE hPort;


int _tmain(int argc, _TCHAR* argv[])
{
	unsigned readbytes, commstate;
	DWORD dwCommModemStatus;
	DCB dcb;
	unsigned char buffer[4096];

	hPort = CreateFile(
		argv[1],
		GENERIC_WRITE | GENERIC_READ,
		0,
		NULL,
		OPEN_EXISTING,
		0,
		//FILE_FLAG_OVERLAPPED,
		NULL
	);

	if (!GetCommState(hPort,&dcb)) {
		printf("Failed to open port\n");
		return 0;
	}
	dcb.BaudRate = CBR_19200; //19200 Baud
	dcb.ByteSize = 8; //8 data bits
	dcb.Parity = NOPARITY; //no parity
	dcb.StopBits = ONESTOPBIT; //1 stop

	   // Update parameters per Dave Andruczyk and Fastest95PGT, see msefi.com t=23071.
   dcb.fAbortOnError     = false;               // Don't abort
   dcb.fBinary           = true;                // Enable binary mode
   dcb.fDsrSensitivity   = false;               // Ignore DSR line
   dcb.fDtrControl       = DTR_CONTROL_DISABLE; // Disable DTR line
   dcb.fErrorChar        = false;               // Don't replace bad chars
   dcb.fInX              = false;               // Disable XIN
   dcb.fNull             = false;               // Don't drop NULL bytes
   dcb.fOutX             = false;               // Disable XOFF
   dcb.fOutxCtsFlow      = false;               // Don't monitor CTS line
   dcb.fOutxDsrFlow      = false;               // Don't monitor DSR line
   dcb.fParity           = false;               // Disabled
   dcb.fRtsControl       = RTS_CONTROL_DISABLE;
   dcb.wReserved         = false;               // As per msdn.  Beware, not a member of _DCB in 8.0 and later header files.

   	 if (!SetupComm(hPort, 8192, 4096)) {
		 printf("Failed to set port buffers\n");
		 CloseHandle(hPort);
		 return 0;
	 }

	if (!SetCommState(hPort,&dcb)) {
		printf("Failed to set baud\n");
		CloseHandle(hPort);
		return 0;
	}
	readbytes = 0;

	while(1) {
		DWORD bytes;
		SetCommMask (hPort, EV_RXCHAR | EV_ERR); //receive character event
		WaitCommEvent (hPort, &dwCommModemStatus, 0); //wait for character
		ReadFile (hPort, buffer + readbytes, 1, &bytes, 0);
		if(bytes) {
			if(buffer[readbytes] == 0x80) { // Start byte
				printf("Start byte\n");
				readbytes = 0;
			} else if(buffer[readbytes] == 0x40) {
				printf("Read %d bytes\n", readbytes);
				for(unsigned i = 0; (i + 4) < readbytes; i += 5) {
					unsigned addr = buffer[i] & 0x3F;
					addr = addr << 6;
					addr |= (buffer[i+1] & 0x3F);
					unsigned char inst = buffer[i+2];
					unsigned data = buffer[i+3] & 0x3F;
					data = data << 6;
					data |= (buffer[i+4] & 0x3F);
					printf("Type %d inst %d value %d\n", addr, inst, data);
				}
			} else {
				readbytes += bytes;
			}
		}
		if(readbytes > 4090) readbytes = 0;
	}

	return 0;
}

